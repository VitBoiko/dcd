<?php

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

// require Composer's autoloader
require __DIR__.'/autoload.php';

class AppKernel extends Kernel
{
    use MicroKernelTrait;

    public function registerBundles()
    {
        return array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new VitaliiBoiko\DcdBundle\VitaliiBoikoDcdBundle()
        );
    }

    protected function configureContainer(ContainerBuilder $c, LoaderInterface $loader)
    {
        if(isset($_ENV['PATH_TO_DCD_CONFIG']))
        {
            $loader->load($_ENV['PATH_TO_DCD_CONFIG']);
        } else {
            $loader->load(__DIR__.'/config/config.yml');
        }
    }

    protected function configureRoutes(RouteCollectionBuilder $routes)
    {

    }
    public function getCacheDir()
    {
        return '/tmp/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return '/tmp/var/logs';
    }
}