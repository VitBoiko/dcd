<?php

namespace VitaliiBoiko\DcdBundle\Helper;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use VitaliiBoiko\DcdBundle\Exception\DirNotFoundException;
use VitaliiBoiko\DcdBundle\Exception\ProjectDirExistException;


/**
 * Class VcsLoaderHelper
 * @package VitaliiBoiko\DcdBundle\Helper
 * @author Vitalii Boiko <vitaliyboyko@i.ua>
 * @link vb.km.ua
 */
class VcsLoaderHelper
{
    /**
     * @var InputInterface
     */
    private $_input;
    /**
     * @var Filesystem
     */
    private $_filesystem;
    /**
     * @var OutputInterface
     */
    private $_output;
    /**
     * @var array
     */
    private $_vcs_aliases;
    /**
     * @var string
     */
    private $_default_vcs_repository;

    /**
     * VcsLoaderHelper constructor.
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param array $vcs_aliases
     * @param $default_vcs_repository string
     */
    public function __construct(InputInterface $input, OutputInterface $output, array $vcs_aliases, $default_vcs_repository)
    {
        $this->_input = $input;
        $this->_filesystem = new Filesystem();
        $this->_output = $output;
        $this->_vcs_aliases = $vcs_aliases;
        $this->_default_vcs_repository = $default_vcs_repository;
    }

    /**
     * @param $app_name string
     * @return string
     * @throws DirNotFoundException
     * @throws ProjectDirExistException
     */
    public function createProjectDirectory($app_name){
        $dir = $this->_input->getOption('directory');

        if ($dir){
            if(is_dir($dir)){
                $dir_name = $dir . '/' . $app_name;
            } else {
                throw new DirNotFoundException($dir);
            }

        } else {
            $dir_name = getcwd() . '/' . $app_name;
        }

        if($this->_filesystem->exists($dir_name)){
            throw new ProjectDirExistException();
        } else {
            $this->_filesystem->mkdir($dir_name);
            $this->_output->writeln('Project directory created!');
        }

        return $dir_name;
    }

    /**
     * @return mixed|string
     * @throws \Exception
     */
    public function getRepository(){
        $repo = $this->_input->getOption('repo');
        $alias = $this->_input->getOption('alias');

        if($repo && $alias){
            throw new \Exception('Yo cant use repo and alias in same time!');
        } elseif ($repo){
            return $repo;
        } elseif ($alias) {
            if(array_key_exists($alias,$this->_vcs_aliases)){
                return $this->_vcs_aliases[$alias];
            } else {
                throw new \Exception('Alias not exist in config!');
            }
        } else {
            return $this->_default_vcs_repository;
        }
    }

    /**
     * @param $repository string
     * @param $directory string
     */
    public function downloadFilesFromRepo($repository, $directory){
        $process = new Process('git clone ' . $repository . ' '. $directory);
        try {
            $process->mustRun();

            print $process->getOutput();
            $this->_output->writeln('Files cloned from repository!');
        } catch (ProcessFailedException $e) {
            $this->_output->writeln($e->getMessage());
            $this->_filesystem->remove($directory);
            die();
        }
    }

}