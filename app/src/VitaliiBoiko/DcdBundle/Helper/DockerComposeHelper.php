<?php

namespace VitaliiBoiko\DcdBundle\Helper;


use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DockerComposeHelper
{
    private $_output;

    public function __construct(OutputInterface $output)
    {
        $this->_output = $output;
    }

    public function dockerIps(){
        $this->run('sed -i.old -r \'/docker-ips/d\' /etc/hosts

docker inspect -f "{{.NetworkSettings.IPAddress}}   {{.Name}}.dev   #docker-ips" $(docker ps -q) | sed -e \'s/\///\' >> /etc/hosts
');
    }

    public function dockerShort(){
        $this->run('sed -i.old -r \'/docker-a/d\' ~/.bashrc

docker inspect -f "alias {{.Name}}=\'docker exec -it {{.Name}} bash\' #docker-a" $(docker ps -q) | sed -e \'s/\///g\' >> ~/.bashrc
');
    }

    public function dockerComposeUp()
    {
        $this->run('docker-compose up -d');
    }

    public function dockerComposeBuild()
    {
        $this->run('docker-compose build');
    }

    public function dockerComposeDown(){
        $this->run('docker-compose down');
    }

    public function reloadBashrc(){
        //TODO: make it works
        exec('. ~/.bashrc');
    }

    private function run($process)
    {
        $process = new Process($process);
        $process->setTimeout(2000);

        $process->run(function ($type, $buffer) {
            $this->_output->writeln($buffer);
            }
        );
    }
}