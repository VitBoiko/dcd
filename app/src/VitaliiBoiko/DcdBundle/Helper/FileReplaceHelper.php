<?php

namespace VitaliiBoiko\DcdBundle\Helper;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FileReplaceHelper
 * @package VitaliiBoiko\DcdBundle\Helper
 * @author Vitalii Boiko <vitaliyboyko@i.ua>
 * @link vb.km.ua
 */
class FileReplaceHelper
{
    /**
     * @var string
     */
    private $path;
    /**
     * @var string
     */
    private $replacement;
    /**
     * @var string
     */
    private $what_replace;
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * FileReplaceHelper constructor.
     * @param $path
     * @param $what_replace
     * @param $replacement
     * @param OutputInterface $output
     */
    public function __construct($path, $what_replace, $replacement, OutputInterface $output)
    {
        $this->path = $path;
        $this->replacement = $replacement;
        $this->what_replace = $what_replace;
        $this->output = $output;
    }

    /**
     * @param string $path
     */
    public function replace($path = ''){
        if (!$path){
            $path = $this->path;
        }

        $handle = opendir($path);

        while ($file = readdir($handle)) {
            if($file != '.' && $file != '..') {

                if(is_dir("$path/$file")) {
                    $this->replace("$path/$file");
                } else {
                    $file_stuff = file_get_contents("$path/$file");
                    file_put_contents("$path/$file", preg_replace("/".$this->what_replace."/",$this->replacement,$file_stuff));
                }
                $this->output->write('.');
            }
        }
        $this->output->writeln(' - ok');

        closedir($handle);
    }
}