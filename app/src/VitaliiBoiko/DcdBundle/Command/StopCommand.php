<?php

namespace VitaliiBoiko\DcdBundle\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use VitaliiBoiko\DcdBundle\Helper\DockerComposeHelper;

class StopCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('stop')

            ->setDescription('Stop a project.')

            ->setHelp('This command allows you to stop and remove docker-compose containers...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $docker_compose = new DockerComposeHelper($output);
        $docker_compose->dockerComposeDown();
    }
}
