<?php

namespace VitaliiBoiko\DcdBundle\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use VitaliiBoiko\DcdBundle\Helper\DockerComposeHelper;

class ReloadCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('reload')

            ->setDescription('Reload a project.')

            ->setHelp('This command allows you to reload a docker-compose...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $docker_compose = new DockerComposeHelper($output);
        $docker_compose->dockerComposeDown();
        $docker_compose->dockerComposeUp();
        $docker_compose->dockerIps();
        $docker_compose->dockerShort();
        $docker_compose->reloadBashrc();
    }
}
