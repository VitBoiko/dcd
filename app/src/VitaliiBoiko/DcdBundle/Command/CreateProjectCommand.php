<?php

namespace VitaliiBoiko\DcdBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use VitaliiBoiko\DcdBundle\Helper\FileReplaceHelper;
use VitaliiBoiko\DcdBundle\Helper\VcsLoaderHelper;

/**
 * Class CreateProjectCommand
 * @package VitaliiBoiko\DcdBundle\Command
 * @author Vitalii Boiko <vitaliyboyko@i.ua>
 * @link vb.km.ua
 */
class CreateProjectCommand extends ContainerAwareCommand
{
    /**
     * This command allows you to create a project...
     */
    protected function configure()
    {
        $this
            ->setName('create')

            ->setDescription('Creates a new project.')

            ->setHelp('This command allows you to create a project...')

            ->addArgument('n', InputArgument::REQUIRED, 'The name of the project.')
            ->addOption('alias', 'a', InputOption::VALUE_REQUIRED, '(Optional) Alias of vcs repository url.' )
            ->addOption('repo', 'r', InputOption::VALUE_REQUIRED, '(Optional) Vcs repository.' )
            ->addOption('directory', 'd', InputOption::VALUE_REQUIRED, '(Optional) Directory.' )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $aliases = $this->getContainer()->getParameter(('vcs_aliases'));
        $default_vcs_repository = $this->getContainer()->getParameter(('default_vcs_repository'));
        $replace_pattern = $this->getContainer()->getParameter(('replace'));

        $vcs_loader = new VcsLoaderHelper($input, $output, $aliases, $default_vcs_repository);

        $app_name = $input->getArgument('n');
        $directory = $vcs_loader->createProjectDirectory($app_name);
        $repository = $vcs_loader->getRepository();
        $vcs_loader->downloadFilesFromRepo($repository, $directory);
        $replace = new FileReplaceHelper($directory,$replace_pattern, $app_name, $output);
        $replace->replace();
        $output->writeln('Job done!');
    }

}