<?php

namespace VitaliiBoiko\DcdBundle\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use VitaliiBoiko\DcdBundle\Helper\DockerComposeHelper;

/**
 * Class StartCommand
 * @package VitaliiBoiko\DcdBundle\Helper
 * @author Vitalii Boiko <vitaliyboyko@i.ua>
 * @link vb.km.ua
 */
class StartCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('start')

            ->setDescription('Run under root. This command allows you to start a docker-compose...')

            ->setHelp('Run under root. This command allows you to start a docker-compose...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $docker_compose = new DockerComposeHelper($output);
        $docker_compose->dockerComposeUp();
        $docker_compose->dockerIps();
        $docker_compose->dockerShort();
        $docker_compose->reloadBashrc();
    }
}