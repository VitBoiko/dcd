<?php

namespace VitaliiBoiko\DcdBundle\Exception;

use Throwable;

/**
 * Class DirNotFoundException
 * @package VitaliiBoiko\DcdBundle\Exception
 * @author Vitalii Boiko <vitaliyboyko@i.ua>
 * @link vb.km.ua
 */
class DirNotFoundException extends \Exception
{
    /**
     * DirNotFoundException constructor.
     * @param string $dir
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($dir, $code = 0, Throwable $previous = null)
    {
        parent::__construct("Wrong option! " . $dir ." - is not a directory!", $code, $previous);
    }
}