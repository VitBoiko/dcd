<?php

namespace VitaliiBoiko\DcdBundle\Exception;

/**
 * Class ProjectDirExistException
 * @package VitaliiBoiko\DcdBundle\Exception
 * @author Vitalii Boiko <vitaliyboyko@i.ua>
 * @link vb.km.ua
 */
class ProjectDirExistException extends \Exception
{
    /**
     * @var string
     */
    protected $message = "Project directory already exist!";
}